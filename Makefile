XML2RFC=	tclsh8.4 xml2rfc/xml2rfc.tcl

XML2TXT=	${XML2RFC} xml2txt
XML2HTML=	${XML2RFC} xml2html

SRC_XML=	draft-levine-dnsextlang.xml

TARG_XML=

ALL_XML=	${SRC_XML} ${TARG_XML}

TARG_FMT=	${ALL_XML:.xml=.fmt}
TARG_HTML=	${ALL_XML:.xml=.html}
TARG_TXT=	${ALL_XML:.xml=.txt}
TARG_PS=	${ALL_XML:.xml=.ps}
TARG_PDF=	${ALL_XML:.xml=.pdf}

TARGETS=	${TARG_XML} ${TARG_FMT} ${TARG_HTML} ${TARG_TXT} ${TARG_PS} ${TARG_PDF}

.SUFFIXES: 	.fmt .html .txt .xml .ps .pdf

# main rules

all:		${TARGETS}

clean:
	rm -f	${TARGETS}
	rm -f	xml2rfc id2fmt fmt2ps
	rm -f	draft-???????~

upload:
	git gc --aggressive
	git update-server-info
	echo "a web frontend to DNS UPDATE" >.git/description
	(echo -n "<pre>"; cat README; echo "</pre>") >.git/README.html
	touch .git/git-daemon-export-ok
	rsync --recursive --links --delete .git/ chiark:public-git/wrrup.git/

# suffix rules

.xml.txt: xml2rfc
	DISPLAY="" ${XML2TXT} $<

.xml.html: xml2rfc
	DISPLAY="" ${XML2HTML} $<

.txt.fmt: id2fmt
	sed -f id2fmt $< | uniq > $@

.fmt.ps: fmt2ps
	./fmt2ps $< $@

.ps.pdf:
	ps2pdf -sPAPERSIZE=a4 $< $@

# auxiliary stuff

id2fmt: Makefile
	rm -f $@
	echo '/^Internet-Draft .* [0-9][0-9][0-9][0-9]$$/d'	>>$@
	echo '/[[]Page [0-9]*[]]$$/d'				>>$@
	echo '/^/d'						>>$@

fmt2ps: Makefile
	rm -f $@
	echo '#!/bin/sh'							>>$@
	echo 'a2ps -2B -f8.8 -stumble -b"$${1%.fmt} : %s./%s#" -o $$2 < $$1'	>>$@
	chmod +x $@

xml2rfc:
	rm -f $@
	ln -s /home/fanf2/scratch/xml2rfc-1.36 $@

# EOF
